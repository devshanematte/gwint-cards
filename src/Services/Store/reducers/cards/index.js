import { cardsList } from '../../../../Domain'

const initialState = {

	//main list
	list:cardsList,

	//fraction
	fraction:'all',
	class:'all',

}

const cards = (state = initialState, action) => {
	switch (action.type){

		case 'CHANGE_FRACTION' :
			return {
				...state,
				fraction:action.fraction
			}

		case 'CHANGE_CLASS' :
			return {
				...state,
				class:action.class
			}

		case 'SORT_CARDS_FRACTION' :

			let getCards = []

			cardsList.map((item)=>{

				if(item.fractionTags.toLowerCase().indexOf(action.fraction) !== -1 && item.classTags.toLowerCase().indexOf(state.class) !== -1){
					getCards = [...getCards, item]
				}

				return getCards

			})

			return {
				...state,
				list:getCards
			}

		case 'SORT_CARDS_CLASS' :

			let getClassCards = []

			cardsList.map((item)=>{

				if(item.fractionTags.toLowerCase().indexOf(state.fraction) !== -1 && item.classTags.toLowerCase().indexOf(action.class) !== -1){
					getClassCards = [...getClassCards, item]
				}

				return getClassCards

			})

			return {
				...state,
				list:getClassCards
			}

		default :
			return state
	}
}

export default cards