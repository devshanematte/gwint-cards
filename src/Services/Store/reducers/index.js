import { combineReducers } from 'redux'

import app from './app'
import cards from './cards'

export default combineReducers({
	app,
	cards
})
