import { applyMiddleware, compose, createStore } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import logger from 'redux-logger'
import storage from 'redux-persist/lib/storage'
import { createBrowserHistory } from "history"

export default (reducers) => {
  const persistConfig = {
    key: 'root',
    storage,
    blacklist: ['cards']
  }

  const middleware = []
  const enhancers = []

  middleware.push(logger)

  const pReducer = persistReducer(persistConfig, reducers)

  enhancers.push(applyMiddleware(...middleware))

  const store = createStore(pReducer, compose(...enhancers))
  const persistor = persistStore(store)

  const history = createBrowserHistory()

  return {
    store, persistor, history
  }
}

