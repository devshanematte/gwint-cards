
import Containers from './Containers'
import Text from './Text'
import Helpers from './Helpers'

const Styled = {
	Containers,
	Text,
	Helpers
}

export default Styled