import styled from 'styled-components'

//Helpers

const Devider = styled.div`
    width:${ props => props.width ? `${props.width}%` : '100%' };
    height:${ props => props.height ? `${props.height}px` : '1px' };
    background:${ props => props.black ? `rgba(0, 0, 0, 0.32)` : 'rgba(0,0,0,0.2)' };

    margin-top:${ props => props.marginTop ? `${props.marginTop}px` : '' };
    margin-bottom:${ props => props.marginBottom ? `${props.marginBottom}px` : '' };
`

export default {
	Devider
}