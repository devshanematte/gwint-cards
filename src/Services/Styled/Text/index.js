import styled from 'styled-components'

//Text

//fonts
//font-family: 'Ubuntu', sans-serif;

const H1 = styled.h1`
    font-size:32px;
    color:#2a2a2c;
    margin:0;
    font-weight:bold;
    text-transform: ${props => props.transform ? 'uppercase' : ''};
    letter-spacing: 1px;
    font-family: 'Ubuntu', sans-serif;
`

const H2 = styled.h2`
    font-size:24px;
    color:rgba(0, 0, 0, 0.69);
    margin:0 0 20px 0;
    position:relative;
    font-weight:bold;
    text-transform: ${props => props.transform ? 'uppercase' : ''};
    font-family: 'Ubuntu', sans-serif;

    &:after{
	    content: '';
	    position: absolute;
	    bottom: -4px;
	    left: 0;
	    width: 48px;
	    height: 3px;
	    background: rgba(0, 0, 0, 0.69);
    }
`

const H3 = styled.h3`
	font-size: 15px;
    color: rgba(0, 0, 0, 0.65);
    margin:0 0 5px 0;
    font-weight:bold;
    text-transform:uppercase;
    text-transform: ${props => props.transform ? 'uppercase' : ''};
    font-family: 'Ubuntu', sans-serif;
`

const P = styled.p`
    font-size:${props => props.normal ? '16px' : '14px'};
    color:${props => props.black ? 'rgba(0,0,0,0.89)' : 'rgba(0,0,0,0.7)'};
    font-weight:${props => props.bold ? 'bold' : ''};
    margin:2px 0 0 0;
    padding:0;
    text-transform: ${props => props.transform ? 'uppercase' : ''};
    font-family: 'Ubuntu', sans-serif;
    text-align:${props => props.align ? props.align : ''};
`

const PI = styled.p`
    font-size:${props => props.normal ? '16px' : '14px'};
    font-weight:${props => props.bold ? 'bold' : ''};
    color:${props => props.black ? 'rgba(0,0,0,0.89)' : 'rgba(0,0,0,0.63)'};
    margin-top:${props => props.marginTop ? `${props.marginTop}px` : '2px'};
    margin-bottom:${props => props.marginBottom ? `${props.marginBottom}px` : ''};
    padding:0;
    text-transform: ${props => props.transform ? 'uppercase' : ''};
    font-family: 'Ubuntu', sans-serif;
    font-style: italic;
`

const a = styled.a`
    font-size:${props => props.normal ? '16px' : '14px'};
    color:rgba(0, 0, 0, 0.7);
    margin:0 0 0 0;
    margin-bottom:${props => props.marginBottom ? `${props.marginBottom}px` : ''};
    padding:0;
    text-transform: ${props => props.transform ? 'uppercase' : ''};
    font-family: 'Ubuntu', sans-serif;
    font-style: ${props => props.italic ? 'italic' : ''};
`

export default {
	H1,
	H2,
	H3,
	P,
	a,
	PI
}