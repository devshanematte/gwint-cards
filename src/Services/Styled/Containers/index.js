import styled from 'styled-components'

const bg1 = require('../../../Assets/bgs/1.jpg')

const Container = styled.div`
    width:100%;
    min-height:100vh;
    background:${props => props.background ? `url(${bg1}) center bottom / cover` : '#eee'};
`

const Wraper = styled.div`
    width:100%;
    height:100vh;
    box-sizing:border-box;
    padding:15px 0;
`

const WraperContent = styled.div`
    width:100%;
    height:${props => props.main ? '100%' : ''};
    display:flex;
    background:${props => props.dark ? 'rgba(0,0,0,0.78)' : ''};
    border-radius:10px;
    box-sizing:border-box;
    padding:${props => props.padding ? '15px' : '0px'};
    overflow:${props => props.overflow ? 'hidden' : ''};
    flex-direction:${props => props.row ? 'row' : 'column'};
`

const TopSide = styled.div`
    width:100%;
    height:140px;
    padding:10px;
    display:flex;
    flex-direction:column;
    align-items:center;
    justify-content:center;
    box-sizing: border-box;
    background:rgba(0, 0, 0, 0.64);
    border-bottom: 1px solid rgba(255, 255, 255, 0.05);

    h1{
        margin:0;
        padding:0;
        color: rgb(51, 26, 17);
        text-shadow: 2px 2px 0px #462d1f;
        font-size: 60px;
        letter-spacing: 8px;
        text-transform:uppercase;
        font-family: 'Alatsi', sans-serif;
        font-weight: bold;
    }

    .fractions-block {
        display:flex;
        flex-direction:row;
        justify-content:center;
        position:relative;
        padding:10px 0;
        align-items:center;
        width:40%;
        margin-top:0;

        h4 {
            margin:0;
            font-family: 'Anton', sans-serif;
            font-size:25px;
            color: rgba(154, 59, 22, 0.74);
        }

        &:after{
            content:'';
            position:absolute;
            top:-4px;
            width:100%;
            display:flex;
            justify-content:center;
            font-family: 'Alatsi', sans-serif;
            font-size: 12px;
            text-transform: uppercase;
            color: rgba(101, 61, 47, 0.75);
        }
    }

`

const Fraction = styled.span`

    & {
        display: block;
        width: 45px;
        height: 45px;
        cursor: pointer;
        background: ${props => props.bgFraction ? `url(${props.bgFraction}) center / contain no-repeat` : ''};
        margin: 0 5px;
        position:relative;
        z-index:1000;
        border:3px solid transparent;
        border-radius: 4px;
        transition:.3s all;
    }

    &.selected {
        border: 3px solid #0f5430;
    }

    &:hover {
        border: 3px solid #0f5430;
        transition:.3s all;
    }

    &:before {
        content: '${props => props.titleFraction}';
        position:absolute;
        text-align:center;
        top:125%;
        display:flex;
        box-sizing:border-box;
        padding:5px 10px;
        background: #351d15;
        color: rgba(255, 255, 255, 0.36);
        text-transform: uppercase;
        font-family: 'Acme', sans-serif;
        font-weight:bold;
        border-radius:5px;
        left:${props => props.left ? `-${props.left}%` : '-10%'};
        opacity:0;
        visibility:hidden;
        transition:.3s all;
    }

    &:hover:before {
        content: '${props => props.titleFraction}';
        opacity:1;
        visibility:visible;
        transition:.3s all;
    }   
`

const Troop = styled.span`

    & {
        display: block;
        width: 55px;
        height: 55px;
        cursor: pointer;
        opacity:0.3;
        background: ${props => props.bgFraction ? `url(${props.bgFraction}) center / contain no-repeat` : '#4c270b'};
        margin: 0 auto 20px auto;
        position:relative;
        box-shadow: 0 0 20px transparent;
        border-radius: 50%;
        transition:.3s all;
    }

    &:after {
        content:'Все';
        position:absolute;
        width:100%;
        height:100%;
        text-transform:uppercase;
        display:flex;
        justify-content:center;
        align-items:center;
        color: rgba(178, 133, 80, 0.52);
        letter-spacing: 1px;
        font-family: 'Anton', sans-serif;
        font-weight:bold;
        opacity:${props => props.bgFraction ? `0` : '1'};
    }

    &.selected {
        opacity:1;
        box-shadow: 0 0 20px #b73203;
        transition:.3s all;   
    }

    &:hover {
        opacity:1;
        box-shadow: 0 0 20px #b73203;
        transition:.3s all;
    }

`


const LeftSide = styled.div`
    width: 100px;
    height: 100%;
    padding:10px;
    background: rgba(0, 0, 0, 0.64);
    box-sizing: border-box;
    border-right: 1px solid rgba(255, 255, 255, 0.05);
`

const WraperCards = styled.div`
    display: grid;
    grid-gap: 3rem;
    grid-template-columns: auto auto auto auto auto;
    grid-auto-rows: minmax(350, 470);
    width: 100%;
    border-radius:10px;
    box-sizing:border-box;
    padding:${props => props.padding ? '15px' : '0px'};
    overflow:${props => props.overflow ? 'hidden' : ''};
    flex-direction:${props => props.row ? 'row' : 'column'};
    overflow:${props => props.scroll ? 'auto' : 'hidden'};


    &::-webkit-scrollbar {
        width: 6px;
        background-color: transparent;
    }
`

const Card = styled.img`
    width:100%;
    height:auto;
    max-height:700px;
    display:table;
    border-radius:20px;
    position:relative;
    cursor:pointer;
    opacity:0.7;
    box-shadow: 0 0 20px transparent;
    transform:scale(1);
    transition:.3s all;

    &:hover {
        transform:scale(1.04);
        opacity:1;
        box-shadow: 0 0 30px #e87b02;
        transition:.3s all;
    }

`

const CardRelativeBlock = styled.div`
    width:100%;
    height:${props => props.overflow ? '100%' : 'auto'};
    display:${props => props.overflow ? '' : 'table'};
    position:${props => props.overflow ? 'absolute' : 'relative'};;
    overflow:${props => props.overflow ? 'hidden' : ''};
`

const LoadingBlock = styled.div`
    position:absolute;
    top:0;
    left:0;
    width:100%;
    height:100%;
    z-index:500;
    background:#330804;
    display:flex;
    justify-content:center;
    align-items:center;
    border-radius:20px;

    h3 {
        text-transform: uppercase;
        font-family: 'Acme',sans-serif;
        font-size: 24px;
        color: #be8e55;
        font-weight: bold;
        text-shadow: 2px 3px 9px #000;
    }
`

const CardDetail = styled.div`
    position:absolute;
    top:0;
    left:0;
    width:100%;
    height:100%;
    z-index:${props => props.zIndex ? props.zIndex : 500};
    background:rgba(0, 0, 0, 0.9215686274509803);
    display:flex;
    box-sizing:border-box;
    padding:15px;
    display:flex;
    flex-direction:column;
    justify-content:center;
    align-items:center;
    border-radius:20px;
    cursor:pointer;
    transform:scale(0);
    transition:.4s all;

    &.cardDetailOpen {
        transform:scale(1);
        transition:.4s all;
    }
`

const CardDetailSection = styled.div`
    display:flex;
    width:100%;

    h5 {
        margin-top:30px;
        color:rgba(255,255,255,0.57);
        font-size:15px;
        font-family: 'Anton',sans-serif;
    }

    p{
        color: #cc995a;
        margin: 0;
        font-size: 13px;
        font-family: 'Anton',sans-serif;
        font-weight: bold;
        letter-spacing: 0px;

        span {
            color: #0f5430;
            font-size: 16px;
            font-weight: bold;
            font-family: 'Acme', sans-serif;
        }
    }
`

export default {
	Container,
	Wraper,
	WraperContent,
	TopSide,
    LeftSide,
    WraperCards,
    Fraction,
    Troop,
    Card,
    CardRelativeBlock,
    LoadingBlock,
    CardDetail,
    CardDetailSection
}