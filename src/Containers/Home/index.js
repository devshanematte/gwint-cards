import React from 'react'
import { connect } from 'react-redux'

import Styled from '../../Services/Styled'
import { Card } from '../../Components'

import TopSide from './Topside'
import LeftSide from './LeftSide'

class Home extends React.Component {

	render(){

		const { dispatch, fraction, cards, cardClass } = this.props

		return(
			<Styled.Containers.Container background>
				
				<div className="col-md-10 col-md-offset-1">
					<Styled.Containers.Wraper>
						<Styled.Containers.WraperContent dark overflow main >

							<TopSide fraction={fraction} dispatch={dispatch} Styled={Styled} />

							<Styled.Containers.WraperContent row main>

								<LeftSide cardClass={cardClass} dispatch={dispatch} Styled={Styled} />

								<Styled.Containers.WraperCards scroll padding row>

									{
										cards.map((item, index)=>{
											return <Card key={index} Styled={Styled} item={item} />
										})
									}

								</Styled.Containers.WraperCards>

							</Styled.Containers.WraperContent>

						</Styled.Containers.WraperContent>
					</Styled.Containers.Wraper>	
				</div>

			</Styled.Containers.Container>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		app:state.app,
		fraction:state.cards.fraction,
		cardClass:state.cards.class,
		cards:state.cards.list,
	}
}

export default connect(mapStateToProps)(Home)