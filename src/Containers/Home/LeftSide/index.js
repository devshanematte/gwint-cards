import React from 'react'

class LeftSide extends React.PureComponent {
	
	saveClass(classCards){

		const { dispatch } = this.props

		dispatch({
			type:'CHANGE_CLASS',
			class:classCards
		})

		dispatch({
			type:'SORT_CARDS_CLASS',
			class:classCards
		})

		return

	}	

	render(){

		const { Styled, cardClass } = this.props

		return(
			<Styled.Containers.LeftSide>
				
				<Styled.Containers.Troop className={cardClass === 'all' ? 'selected' : ''} onClick={this.saveClass.bind(this, 'all')} troops="hand_units" />
				<Styled.Containers.Troop className={cardClass === 'hand_units' ? 'selected' : ''} onClick={this.saveClass.bind(this, 'hand_units')} troops="hand_units" bgFraction={require('../../../Assets/icons/hand_units.png')} />
				<Styled.Containers.Troop className={cardClass === 'long_distance' ? 'selected' : ''} onClick={this.saveClass.bind(this, 'long_distance')} troops="long_distance" bgFraction={require('../../../Assets/icons/long_distance.png')} />
				<Styled.Containers.Troop className={cardClass === 'siege_troops' ? 'selected' : ''} onClick={this.saveClass.bind(this, 'siege_troops')} troops="siege_troops" bgFraction={require('../../../Assets/icons/siege_troops.png')} />

			</Styled.Containers.LeftSide>
		)
	}
}

export default LeftSide