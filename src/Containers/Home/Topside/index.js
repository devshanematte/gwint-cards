import React from 'react'

class TopSide extends React.PureComponent {

	saveFraction(fraction){

		const { dispatch } = this.props

		dispatch({
			type:'CHANGE_FRACTION',
			fraction
		})

		dispatch({
			type:'SORT_CARDS_FRACTION',
			fraction
		})

		return

	}

	render(){

		const { Styled, fraction } = this.props

		return(
			<Styled.Containers.TopSide>
				
				<h1>gwint cards</h1>

				<div className="fractions-block">
					
					<Styled.Containers.Fraction fraction="all" className={fraction === 'all' ? 'selected' : ''} onClick={this.saveFraction.bind(this, 'all')} titleFraction="Все" bgFraction={require('../../../Assets/icons/all.png')} />

					<Styled.Containers.Fraction fraction="monsters" className={fraction === 'monsters' ? 'selected' : ''} onClick={this.saveFraction.bind(this, 'monsters')} titleFraction="Чудовища" left={75} bgFraction={require('../../../Assets/icons/monsters.png')} />

					<Styled.Containers.Fraction fraction="neutrals" className={fraction === 'neutrals' ? 'selected' : ''} onClick={this.saveFraction.bind(this, 'neutrals')} titleFraction="Нейтралитет" left={105} bgFraction={require('../../../Assets/icons/neutrals.png')} />

					<Styled.Containers.Fraction fraction="nilfgard" className={fraction === 'nilfgard' ? 'selected' : ''} onClick={this.saveFraction.bind(this, 'nilfgard')} titleFraction="Нильфгаард" left={95} bgFraction={require('../../../Assets/icons/nilfgard.png')} />

					<Styled.Containers.Fraction fraction="northern_kingdom" className={fraction === 'northern_kingdom' ? 'selected' : ''} onClick={this.saveFraction.bind(this, 'northern_kingdom')} titleFraction="Королевства Севера" left={105} bgFraction={require('../../../Assets/icons/northern_kingdom.png')} />

					<Styled.Containers.Fraction fraction="scoyataels" className={fraction === 'scoyataels' ? 'selected' : ''} onClick={this.saveFraction.bind(this, 'scoyataels')} titleFraction="Скоя’таэли" left={85} bgFraction={require('../../../Assets/icons/scoyataels.png')} />

					<Styled.Containers.Fraction fraction="skelling" className={fraction === 'skelling' ? 'selected' : ''} onClick={this.saveFraction.bind(this, 'skelling')} titleFraction="Скеллиге" left={65} bgFraction={require('../../../Assets/icons/skelling.png')} />

				</div>

			</Styled.Containers.TopSide>
		)
	}
}

export default TopSide