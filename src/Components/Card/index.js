import React from 'react'

class Card extends React.PureComponent {

	constructor(props){
		super(props)

		this.state = {
			loading:false,
			detail:false,
			getCardInfo:false
		}
	}

	handleImageLoaded(){
		this.setState({
			loading:true
		})
	}

	render(){

		const { Styled, item } = this.props
		const { loading, detail, getCardInfo } = this.state

		return(
			<Styled.Containers.CardRelativeBlock>

				<Styled.Containers.CardRelativeBlock overflow 
					onMouseLeave={()=>{this.setState({
						detail:false,
						getCardInfo:false
					})}}
				>

					<Styled.Containers.CardDetail zIndex={501} className={getCardInfo ? 'cardDetailOpen' : ''} onClick={()=>{ this.setState({getCardInfo:!this.state.getCardInfo}) }}>

						{
							item.howToGet ?
								<Styled.Containers.CardDetailSection>
									<p><span>Как получить: </span>{ item.howToGet }</p>
								</Styled.Containers.CardDetailSection>
							:
								<Styled.Containers.CardDetailSection>
									<p>Нет информации</p>
								</Styled.Containers.CardDetailSection>	
						}

					</Styled.Containers.CardDetail>

					<Styled.Containers.CardDetail zIndex={500} className={detail ? 'cardDetailOpen' : ''}>

						{
							item.abilityName ?
								<Styled.Containers.CardDetailSection>
									<p><span>{ item.abilityName }: </span>{ item.ability }</p>
								</Styled.Containers.CardDetailSection>
							:
								<Styled.Containers.CardDetailSection>
									<p>Нет способностей</p>
								</Styled.Containers.CardDetailSection>	
						}

						<Styled.Containers.CardDetailSection>
							<h5 onClick={()=>{ this.setState({getCardInfo:!this.state.getCardInfo}) }}>Как получить?</h5>
						</Styled.Containers.CardDetailSection>

					</Styled.Containers.CardDetail>
				</Styled.Containers.CardRelativeBlock>
				<Styled.Containers.Card
					onClick={()=>{ this.setState({detail:!this.state.detail}) }}
					poster={item.poster} 
					src={item.poster}
					onLoad={this.handleImageLoaded.bind(this)}
				/>

				{
					!loading ?
						<Styled.Containers.LoadingBlock>
							<h3>Загрузка</h3>
						</Styled.Containers.LoadingBlock>
					:
						<div></div>
				}
			</Styled.Containers.CardRelativeBlock>
		)
	}
}

export default Card